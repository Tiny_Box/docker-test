FROM php:5.6-fpm
MAINTAINER tiny_box "tiny_box@outlook.com"

# Install env
ADD sources.list    /etc/apt/sources.list
COPY xdebug.tgz /home/xdebug.tgz

RUN pecl install /home/xdebug.tgz && echo "zend_extension=/usr/local/lib/php/extensions/no-debug-non-zts-20131226/xdebug.so" > /usr/local/etc/php/conf.d/xdebug.ini

COPY ./build.sh /
RUN /build.sh

# PHP config
ADD php.ini    /usr/local/etc/php/php.ini
ADD php-fpm.conf    /usr/local/etc/php-fpm.conf

# Composer
ADD composer.phar /usr/local/bin/composer
RUN chmod 755 /usr/local/bin/composer

# Write Permission
RUN usermod -u 1000 www-data

EXPOSE 9000

# prepare dir
RUN mkdir /source

WORKDIR /source
