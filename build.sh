#!/bin/bash

# install dependencies
apt-get update && 
apt-get install -y --no-install-recommends \
libopencv-dev yasm libjpeg-dev libjasper-dev libavcodec-dev libavformat-dev \
libswscale-dev libdc1394-22-dev libv4l-dev libtbb-dev \
libqt4-dev libgtk2.0-dev libmp3lame-dev libopencore-amrnb-dev \
libopencore-amrwb-dev libtheora-dev libvorbis-dev libxvidcore-dev x264 v4l-utils \
pkg-config build-essential cmake wget checkinstall\

# install opencv
# ===========================
curl -sL https://github.com/Itseez/opencv/archive/2.4.9.tar.gz | tar xvz -C /tmp
mkdir -p /tmp/opencv-2.4.9/build

cd /tmp/opencv-2.4.9/build

cmake -DWITH_FFMPEG=OFF -DWITH_OPENEXR=OFF -DBUILD_TIFF=OFF -DWITH_CUDA=OFF -DWITH_NVCUVID=OFF -DBUILD_PNG=OFF ..
make
make install
# ==========================

# install imagemagic
# ==========================
cd /tmp/
wget https://sourceforge.net/projects/imagemagick/files/6.9.5-sources/ImageMagick-6.9.5-3.tar.gz
tar -zxf ImageMagick-6.9.5-3.tar.gz
cd ImageMagick-6.9.5-3
apt-get build-dep libmagick++-dev -y --no-install-recommends
./configure --disable-static --with-modules --without-perl --without-magick-plus-plus --with-quantum-depth=8 --with-gs-font-dir=/usr/share/fonts/type1/gsfonts/
make
make install
# ==========================

# configure
echo "/usr/local/lib" > /etc/ld.so.conf.d/opencv.conf
ldconfig
cp /usr/local/lib/pkgconfig/opencv.pc /usr/share/pkgconfig/
ln /dev/null /dev/raw1394 # hide warning - http://stackoverflow.com/questions/12689304/ctypes-error-libdc1394-error-failed-to-initialize-libdc1394

# cleanup package manager
apt-get remove --purge -y curl build-essential checkinstall wget
apt-get autoclean && apt-get clean
rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
